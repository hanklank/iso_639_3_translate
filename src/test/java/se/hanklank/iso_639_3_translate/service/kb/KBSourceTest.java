package se.hanklank.iso_639_3_translate.service.kb;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import se.hanklank.iso_639_3_translate.model.Entry;
import se.hanklank.iso_639_3_translate.model.KBEntry;
import se.hanklank.iso_639_3_translate.model.PoEntry;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static se.hanklank.iso_639_3_translate.service.ServiceTestUtils.*;

/*
    Truth table for tests
        S O M (source,overwrite, usemsgidassource)
        1 0 0
        1 1 0
        1 0 1
        1 1 1
        0 0 0
        0 0 1
        0 1 0
        0 1 1

*/
@RunWith(MockitoJUnitRunner.class)
public class KBSourceTest {

    @Mock
    private KBClient kbClient;

    private KBSource kbSource;

    private boolean overwriteMsgStr = true;
    private boolean useMsgIdAsSource = true;

    @Before
    public void setup() {

        kbSource = new KBSource(kbClient);
        when(kbClient.getKbData()).thenReturn(createKbData());
    }

    @Test
    public void hasSourceOverwriteIsFalseUseMsgIdAsSourceIsFalse() {
        verifyKbRequestResult(getEntries_msgstr1_xxx_astring_msgstr4(), false, false);
    }

    private void verifyKbRequestResult(List<Entry> verifyEntries, boolean overwriteMsgStr, boolean useMsgIdAsSource) {
        List<PoEntry> poEntries = getPoEntries();
        List<Entry> entries = new ArrayList<>();

        kbSource.process(poEntries, entries, overwriteMsgStr, useMsgIdAsSource);

        verify(kbClient).getKbData();

        assertThat(entries).hasSize(4)
                .usingFieldByFieldElementComparator()
                .containsExactlyElementsOf(verifyEntries);
    }

    @Test
    public void hasSourceOverwriteIsTrueUseMsgIdAsSourceIsFalse() {
        verifyKbRequestResult(createEntries_msgstr1_xxx_msgstr3_msgstr4(), overwriteMsgStr, false);
    }

    @Test
    public void hasSourceOverwriteIsFalseUseMsgIdAsSourceIsTrue() {
        verifyKbRequestResult(getEntries_msgstr1_xxx_astring_msgstr4(), false, useMsgIdAsSource);
    }

    @Test
    public void hasSourceOverwriteIsTrueUseMsgIdAsSourceIsTrue() {
        verifyKbRequestResult(createEntries_msgstr1_xxx_msgstr3_msgstr4(), overwriteMsgStr, useMsgIdAsSource);
    }

    @Test
    public void hasNoSourceOverwriteIsFalseUseMsgIdAsSourceIsFalse() {

        List<PoEntry> poEntries = getPoEntries();
        List<Entry> entries = new ArrayList<>();

        when(kbClient.getKbData()).thenReturn(Collections.emptyMap());

        kbSource.process(poEntries, entries, false, false);

        verify(kbClient).getKbData();

        assertThat(entries).hasSize(4)
                .usingFieldByFieldElementComparator()
                .containsExactlyElementsOf(getOriginalMsgStrEntries());
    }

    private void verifyKbRequestResult_NoSource(List<Entry> verifyEntries, boolean overwriteMsgStr, boolean useMsgIdAsSource) {

        List<PoEntry> poEntries = getPoEntries();
        List<Entry> entries = new ArrayList<>();

        when(kbClient.getKbData()).thenReturn(Collections.emptyMap());

        kbSource.process(poEntries, entries, overwriteMsgStr, useMsgIdAsSource);

        verify(kbClient).getKbData();

        assertThat(entries).hasSize(4)
                .usingFieldByFieldElementComparator()
                .containsExactlyElementsOf(verifyEntries);
    }

    @Test
    public void hasNoSourceOverwriteIsFalseUseMsgIdAsSourceIsTrue() {
        verifyKbRequestResult_NoSource(noOverwriteUseMsgIdSource(), false, useMsgIdAsSource);
    }

    @Test
    public void hasNoSourceOverwriteIsTrueUseMsgIdAsSourceIsFalse() {
        verifyKbRequestResult_NoSource(getOriginalMsgStrEntries(), overwriteMsgStr, false);
    }

    @Test
    public void hasNoSourceOverwriteIsTrueUseMsgIdAsSourceIsTrue() {
        verifyKbRequestResult_NoSource(noKbSourceOverwriteMsgIsAsSource(), overwriteMsgStr, useMsgIdAsSource);
    }


    @Test
    public void hasNoSource_OverwriteTrue_UseMsgIdAsSourceTrue_PoEntriesEmpty() {

        List<PoEntry> poEntries = new ArrayList<>();
        List<Entry> entries = new ArrayList<>();

        kbSource.process(poEntries, entries, overwriteMsgStr, useMsgIdAsSource);

        verify(kbClient).getKbData();

        assertThat(entries).isEmpty();
    }

    private Map<String, KBEntry> createKbData() {
        Map<String, KBEntry> data = new LinkedHashMap<>();

        KBEntry entry1 = KBEntry.builder()
                .swe("msgstr1")
                .isocode("aaa")
                .comment("comment").build();

        KBEntry entry2 = KBEntry.builder()
                .swe("msgstr2")
                .isocode("aab")
                .comment("").build();

        KBEntry entry3 = KBEntry.builder()
                .swe("msgstr3")
                .isocode("aac")
                .comment("").build();

        KBEntry entry4 = KBEntry.builder()
                .swe("msgstr4")
                .isocode("aad")
                .comment("").build();

        data.put("aaa", entry1);
        data.put("aab", entry2);
        data.put("aac", entry3);
        data.put("aad", entry4);

        return data;
    }
}
