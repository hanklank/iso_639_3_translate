package se.hanklank.iso_639_3_translate.service;

import se.hanklank.iso_639_3_translate.PoHandler;
import se.hanklank.iso_639_3_translate.model.Entry;
import se.hanklank.iso_639_3_translate.model.PoEntry;
import se.hanklank.iso_639_3_translate.service.wikipedia.WikipediaSourceTest;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static se.hanklank.iso_639_3_translate.util.Utils.QUOTE;

public class ServiceTestUtils {

    public static Entry getEntry(String entryline, String isocode) {

        return Entry.builder()
                .entryline(entryline)
                .isocode(isocode).build();
    }

    public static List<Entry> getEntries_msgstr1_xxx_astring_msgstr4() {
        return getEntries("msgstr1", "xxx", "astring", "msgstr4");
    }

    private static List<Entry> getEntries(String one, String two, String three, String four) {

        Entry entry = getEntry("\n" +
                "\n" +
                "#. name for aaa, reference_name for aaa\n" +
                "msgid \"aaa\"\n" +
                "msgstr " + QUOTE + one + QUOTE, "aaa");

        Entry entry2 = getEntry("\n" +
                "\n" +
                "#. name for xxx, reference_name for xxx\n" +
                "msgid \"xxx\"\n" +
                "msgstr " + QUOTE + two + QUOTE, "xxx");

        Entry entry3 = getEntry("\n" +
                "\n" +
                "#. name for aac, reference_name for aac\n" +
                "msgid \"aac\"\n" +
                "msgstr " + QUOTE + three + QUOTE, "aac");

        Entry entry4 = getEntry("\n" +
                "\n" +
                "# Another comment row\n" +
                "#. name for aad, reference_name for aad\n" +
                "msgid \"aad\"\n" +
                "msgstr " + QUOTE + four + QUOTE, "aad");

        List<Entry> entries = new ArrayList<>();
        entries.add(entry);
        entries.add(entry2);
        entries.add(entry3);
        entries.add(entry4);
        return entries;
    }

    public static List<Entry> createEntries_msgstr1_xxx_msgstr3_msgstr4() {
        return getEntries("msgstr1", "xxx", "msgstr3", "msgstr4");
    }

    public static List<Entry> createEntries_noOverwrite() {
        return getEntries("msgstr1", "xxx", "aaa", "msgstr4");
    }


    public static List<Entry> noOverwriteUseMsgIdSource() {
        return getEntries("aaa", "xxx", "astring", "aad");
    }

    public static List<Entry> getOriginalMsgStrEntries() {
        return getEntries("", "xxx", "astring", "");
    }

    public static List<Entry> noKbSourceOverwriteMsgIsAsSource() {
        return getEntries("aaa", "xxx", "aac", "aad");

    }

    public static List<PoEntry> getPoEntries() {
        PoHandler poHandler = new PoHandler();
        File iso6393 = new File(WikipediaSourceTest.class.getResource("/sources/iso6393.po").getFile());
        return poHandler.toPoEntries(iso6393.toPath());
    }
}
