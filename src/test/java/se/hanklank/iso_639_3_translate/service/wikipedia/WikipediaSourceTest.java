package se.hanklank.iso_639_3_translate.service.wikipedia;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import se.hanklank.iso_639_3_translate.model.Entry;
import se.hanklank.iso_639_3_translate.model.PoEntry;
import se.hanklank.iso_639_3_translate.service.wikipedia.api.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static se.hanklank.iso_639_3_translate.service.ServiceTestUtils.*;

@RunWith(MockitoJUnitRunner.class)
public class WikipediaSourceTest {

    @Mock
    private WikipediaClient wikipediaClient;
    private WikipediaSource wikipediaSource;

    private boolean overwriteMsgStr = true;
    private boolean useMsgIdAsSource = true;

    @Before
    public void setup() {
        wikipediaSource = new WikipediaSource(wikipediaClient);
        when(wikipediaClient.openSearch(any(PoEntry.class), anyString(), anyString())).thenReturn(of(createOpenSearch()));
    }

    @Test
    public void hasSourceOverwriteIsFalseUseMsgIdAsSourceIsFalse() {
        verifyWikipediaRequestResult(getEntries_msgstr1_xxx_astring_msgstr4(), false, false);
    }

    @Test
    public void hasSourceOverwriteIsTrueUseMsgIdAsSourceIsFalse() {

        verifyWikipediaRequestResult(createEntries_msgstr1_xxx_msgstr3_msgstr4(), overwriteMsgStr, false);
    }

    @Test
    public void hasSourceOverwriteIsFalseUseMsgIdAsSourceIsTrue() {

        verifyWikipediaRequestResult(getEntries_msgstr1_xxx_astring_msgstr4(), false, useMsgIdAsSource);
    }

    @Test
    public void hasSourceOverwriteIsTrueUseMsgIdAsSourceIsTrue() {
        verifyWikipediaRequestResult(createEntries_msgstr1_xxx_msgstr3_msgstr4(), overwriteMsgStr, useMsgIdAsSource);
    }

    @Test
    public void hasNoSourceOverwriteIsFalseUseMsgIdAsSourceIsFalse() {
        verifyWikipediaRequestResult_NoSourceGiven(getOriginalMsgStrEntries(), false, false);
    }

    @Test
    public void hasNoSourceOverwriteIsFalseUseMsgIdAsSourceIsTrue() {
        verifyWikipediaRequestResult_NoSourceGiven(noOverwriteUseMsgIdSource(), false, useMsgIdAsSource);
    }

    @Test
    public void hasNoSourceOverwriteIsTrueUseMsgIdAsSourceIsFalse() {
        verifyWikipediaRequestResult_NoSourceGiven(getOriginalMsgStrEntries(), overwriteMsgStr, false);
    }

    @Test
    public void hasNoSourceOverwriteIsTrueUseMsgIdAsSourceIsTrue() {
        verifyWikipediaRequestResult_NoSourceGiven(noKbSourceOverwriteMsgIsAsSource(), overwriteMsgStr, useMsgIdAsSource);
    }

    @Test
    public void hasNoSource_OverwriteTrue_UseMsgIdAsSourceTrue_PoEntriesEmpty() {

        List<PoEntry> poEntries = new ArrayList<>();
        List<Entry> entries = new ArrayList<>();

        wikipediaSource.process(poEntries, entries, overwriteMsgStr, useMsgIdAsSource);

        verify(wikipediaClient, times(0)).openSearch(any(PoEntry.class), anyString(), anyString());
        verify(wikipediaClient, times(0)).langlinksSearch(anyString(), anyString());

        assertThat(entries).isEmpty();
    }

    private void verifyWikipediaRequestResult(List<Entry> verifyEntries, boolean overwriteMsgStr, boolean useMsgIdAsSource) {

        List<PoEntry> poEntries = getPoEntries();
        List<Entry> entries = new ArrayList<>();

        when(wikipediaClient.langlinksSearch(anyString(), anyString()))
                .thenReturn(of(createLinkhandler("msgstr1")),
                        of(createLinkhandler("xxx")),
                        of(createLinkhandler("msgstr3")),
                        of(createLinkhandler("msgstr4")));

        wikipediaSource.process(poEntries, entries, overwriteMsgStr, useMsgIdAsSource);

        verify(wikipediaClient, times(4)).openSearch(any(PoEntry.class), anyString(), anyString());
        verify(wikipediaClient, times(4)).langlinksSearch(anyString(), anyString());

        assertThat(entries).hasSize(4)
                .usingFieldByFieldElementComparator()
                .containsExactlyElementsOf(verifyEntries);

    }

    private void verifyWikipediaRequestResult_NoSourceGiven(List<Entry> verifyEntries, boolean overwriteMsgStr, boolean useMsgIdAsSource) {

        List<PoEntry> poEntries = getPoEntries();
        List<Entry> entries = new ArrayList<>();

        when(wikipediaClient.openSearch(any(PoEntry.class), anyString(), anyString())).thenReturn(Optional.empty());

        wikipediaSource.process(poEntries, entries, overwriteMsgStr, useMsgIdAsSource);

        verify(wikipediaClient, times(4)).openSearch(any(PoEntry.class), anyString(), anyString());
        verify(wikipediaClient, times(0)).langlinksSearch(anyString(), anyString());

        assertThat(entries).hasSize(4)
                .usingFieldByFieldElementComparator()
                .containsExactlyElementsOf(verifyEntries);
    }

    private static OpenSearch createOpenSearch() {
        List<String> descriptions = new ArrayList<>();
        descriptions.add("description");
        return new OpenSearch("", descriptions, new ArrayList<>());
    }

    private static Linkhandler createLinkhandler(String msgStr) {

        Langlink langLink = new Langlink("lang", msgStr);
        List<Langlink> langLinks = new ArrayList<>();
        langLinks.add(langLink);

        Linkhandler linkhandler = new Linkhandler(new Query(new PageItem(0, "title", langLinks, "0")));
        return linkhandler;

    }
}
