package se.hanklank.iso_639_3_translate;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ISO_639_3_IntegrationTest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    @Test
    public void test() throws Exception {
        File root = folder.getRoot();
        Map<String, Object> urlParams = new HashMap<String, Object>();
        urlParams.put("path", "/tmp/");
        urlParams.put("sourceType", "WIKIPEDIA");


        this.restTemplate.getForEntity("http://localhost:" + this.port +
                "/services/translate?path={path}&sourceType={sourceType}", Void.class, urlParams);

    }
}
