package se.hanklank.iso_639_3_translate;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import se.hanklank.iso_639_3_translate.model.Entry;
import se.hanklank.iso_639_3_translate.model.PoEntry;
import se.hanklank.iso_639_3_translate.util.Utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.contentOf;

public class PoHandlerTest {

    private static String TESTFILE_NAME = "test.po";
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();
    private List<PoEntry> poEntries;
    private File iso6393;
    private PoHandler poHandler;
    private Path testfilePath;

    @Before
    public void setup() throws IOException {
        testfilePath = folder.newFile(TESTFILE_NAME).toPath();
        poHandler = new PoHandler();
        iso6393 = new File(this.getClass().getResource("/sources/iso6393.po").getFile());
        poEntries = poHandler.toPoEntries(iso6393.toPath());
    }

    @Test
    public void testEntryLineIsWritten() throws IOException {
        List<Entry> entries = new ArrayList<>();

        entries.add(Entry.builder()
                .entryline("entry")
                .isocode("iso").build());
        entries.add(Entry.builder()
                .entryline("entry2")
                .isocode("iso").build());

        List<String> strings = entries.stream().map(Entry::getEntryline).collect(toList());

        Utils.write(strings, testfilePath);

        assertThat(contentOf(testfilePath.toFile()).equals("entryentry2"));
    }

    @Test
    public void testEmptyEntryLineDoesntWriteToFile() throws IOException {

        Utils.write(new ArrayList<>(), testfilePath);

        assertThat(Files.notExists(testfilePath));
    }

    @Test
    public void testThatReadAndSavedPoFileHasSameContent() throws IOException {

        List<Entry> entries = new ArrayList<>();
        poEntries.stream()
                .forEachOrdered(poEntry -> entries.add(poEntry.getEntry()));

        List<String> strings = entries.stream().map(Entry::getEntryline).collect(toList());

        Utils.write(strings, testfilePath);

        assertThat(contentOf(testfilePath.toFile())).isEqualTo(contentOf(iso6393));

    }

}
