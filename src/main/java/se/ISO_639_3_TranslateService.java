package se;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import se.hanklank.iso_639_3_translate.PoHandler;
import se.hanklank.iso_639_3_translate.model.Entry;
import se.hanklank.iso_639_3_translate.model.SourceType;
import se.hanklank.iso_639_3_translate.service.SourceFactory;
import se.hanklank.iso_639_3_translate.util.Utils;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

@RequiredArgsConstructor
@Slf4j
@Service
public class ISO_639_3_TranslateService {

    private final PoHandler poHandler;
    private final SourceFactory sourceFactory;

    @Value("${outputpath}")
    private Path outputpath;

    @Async
    public void process(Path path, SourceType sourceType, boolean overwriteMsgStr, boolean useMsgIdAsSource) {

        try {
            List<Entry> targetEntries = new ArrayList<>();

            sourceFactory.getSource(sourceType)
                    .process(poHandler.toPoEntries(path), targetEntries, overwriteMsgStr, useMsgIdAsSource);

            List<String> entries = targetEntries.stream().map(Entry::getEntryline).collect(toList());
            Utils.write(entries, outputpath);

        } catch (IOException e) {
            log.error("Error processing source", e);
        }
    }
}
