package se;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import se.hanklank.iso_639_3_translate.model.SourceType;

import java.nio.file.Paths;

@RequiredArgsConstructor
@RestController
@RequestMapping("/services")
public class ISO_639_3_TranslateController {

    private final ISO_639_3_TranslateService iso6393TranslateService;

    @GetMapping(value = "translate")
    public void translate(@RequestParam String poSourcePath, @RequestParam SourceType sourceType,
                          @RequestParam boolean overwriteMsgStr, @RequestParam boolean useMsgIdAsSource) {
        iso6393TranslateService.process(Paths.get(poSourcePath), sourceType, overwriteMsgStr, useMsgIdAsSource);
    }

}
