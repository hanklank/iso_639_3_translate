package se;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ISO_639_3_TranslateApp {

    public static void main(String[] args) {
        SpringApplication.run(ISO_639_3_TranslateApp.class, args);
    }
}
