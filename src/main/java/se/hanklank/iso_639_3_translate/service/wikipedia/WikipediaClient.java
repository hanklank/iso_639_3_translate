package se.hanklank.iso_639_3_translate.service.wikipedia;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import se.hanklank.iso_639_3_translate.model.PoEntry;
import se.hanklank.iso_639_3_translate.service.wikipedia.api.Linkhandler;
import se.hanklank.iso_639_3_translate.service.wikipedia.api.OpenSearch;

import java.net.URI;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@RequiredArgsConstructor
@Component
public class WikipediaClient {

    private final RestTemplate restTemplate;
    private final ObjectMapper mapper;

    Optional<Linkhandler> langlinksSearch(String lang, String title) {
        URI uri = UriComponentsBuilder
                .fromUriString("https://" + lang + ".wikipedia.org")
                .pathSegment("w")
                .pathSegment("api.php")
                .queryParam("action", "query")
                .queryParam("prop", "langlinks")
                .queryParam("titles", title)

                .queryParam("lllang", "sv")
                .queryParam("format", "json")
                .queryParam("redirects", "")
                .build().toUri();

        return ofNullable(restTemplate.getForObject(uri, Linkhandler.class));
    }


    Optional<OpenSearch> openSearch(PoEntry poEntry, String lang, String langagueterm) {

        URI uri = UriComponentsBuilder
                .fromUriString("https://" + lang + ".wikipedia.org")
                .pathSegment("w")
                .pathSegment("api.php")
                .queryParam("action", "opensearch")
                .queryParam("search", poEntry.getMsgid().trim() + "|" + langagueterm)
                .build().toUri();

        Object[] response = restTemplate.getForObject(uri, Object[].class);

        return ofNullable(mapper.convertValue(response, OpenSearch.class));
    }
}
