package se.hanklank.iso_639_3_translate.service.wikipedia.api;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.google.common.collect.Iterables;

import java.io.IOException;
import java.util.LinkedHashMap;

public class PageItemDeserializer extends JsonDeserializer<PageItem> {

    @Override
    public PageItem deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {

        LinkedHashMap<String, PageItem> pageitemMap = p.readValueAs(getPageItemValueTypeRef());
        return Iterables.getOnlyElement(pageitemMap.values());
    }

    private static TypeReference<LinkedHashMap<String, PageItem>> getPageItemValueTypeRef() {
        return new TypeReference<LinkedHashMap<String, PageItem>>() {
        };
    }
}