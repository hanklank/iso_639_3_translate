package se.hanklank.iso_639_3_translate.service.wikipedia.api;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Value
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PageItem {

    @JsonProperty("pageid")
    private Integer pageid;
    @JsonProperty("title")
    private String title;
    @JsonProperty("langlinks")
    private List<Langlink> langlinks;
    @JsonIgnore
    @JsonProperty("ns")
    private String ns;

    @JsonProperty("langlinks")
    public Optional<List<Langlink>> getLanglinks() {

        return ofNullable(langlinks);
    }
}