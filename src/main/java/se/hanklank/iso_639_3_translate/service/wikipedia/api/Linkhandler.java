package se.hanklank.iso_639_3_translate.service.wikipedia.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.Optional;

@Value
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Linkhandler {

    @JsonProperty("query")
    private final Query query;

    @JsonProperty("query")
    public final Optional<Query> getQuery() {
        return Optional.ofNullable(query);
    }
}
