package se.hanklank.iso_639_3_translate.service.wikipedia.api;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;

import java.util.List;
import java.util.Optional;

import static com.google.common.collect.ImmutableList.copyOf;
import static java.util.Optional.ofNullable;

@Value
@JsonDeserialize(using = OpenSearchDeserializer.class)
public class OpenSearch {

    private final String openSearchTerm;
    private final List<String> descriptions;
    private final List<String> pagelinks;

    public final Optional<List<String>> getDescriptions() {
        return ofNullable((copyOf(descriptions)));
    }
}
