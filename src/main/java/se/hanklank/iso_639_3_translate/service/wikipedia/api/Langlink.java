package se.hanklank.iso_639_3_translate.service.wikipedia.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Langlink {

    @JsonProperty("lang")
    private final String lang;

    @JsonProperty("*")
    private final String star;
}
