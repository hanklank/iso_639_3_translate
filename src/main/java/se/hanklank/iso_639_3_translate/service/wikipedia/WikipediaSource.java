package se.hanklank.iso_639_3_translate.service.wikipedia;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import se.hanklank.iso_639_3_translate.model.Entry;
import se.hanklank.iso_639_3_translate.model.PoEntry;
import se.hanklank.iso_639_3_translate.service.ISO_639_3_Source;
import se.hanklank.iso_639_3_translate.service.wikipedia.api.Langlink;
import se.hanklank.iso_639_3_translate.service.wikipedia.api.Linkhandler;
import se.hanklank.iso_639_3_translate.service.wikipedia.api.OpenSearch;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static se.hanklank.iso_639_3_translate.util.Utils.*;

@RequiredArgsConstructor
@Slf4j
@Component
public class WikipediaSource implements ISO_639_3_Source {

    private static final String LANG = "en";
    private static final String LANGUAGE_TERM = "language";

    private final WikipediaClient wikipediaClient;

    @Override
    public void process(List<PoEntry> poEntries, List<Entry> entries, boolean overwriteMsgStr, boolean useMsgIdAsSource) {

        poEntries.stream()
                .forEachOrdered(poEntry -> {

                    List<String> resultTitles = new ArrayList<>();

                    openSearch(poEntry, LANG, LANGUAGE_TERM)
                            .ifPresent(openSearch -> openSearch.getDescriptions()
                                    .ifPresent(description -> langlinksSearch(openSearch)
                                            .ifPresent(linkhandler -> linkhandler.getQuery()
                                                    .ifPresent(query -> query.getPageItem()
                                                            .ifPresent(pageItem -> pageItem.getLanglinks()
                                                                    .ifPresent(langlink -> getStarFromLanglink(langlink, resultTitles))

                                                            )
                                                    )
                                            )
                                    ));

                    entries.add(createEntry(poEntry, resultTitles, overwriteMsgStr, useMsgIdAsSource));
                });
    }

    private Optional<OpenSearch> openSearch(PoEntry poEntry, String lang, String langterm) {
        return wikipediaClient.openSearch(poEntry, lang, langterm);
    }

    private Optional<Linkhandler> langlinksSearch(OpenSearch openSearch) {
        return wikipediaClient.langlinksSearch(LANG, openSearch.getDescriptions().get().get(0));
    }

    private Entry createEntry(PoEntry poEntry, List<String> resultTitles, boolean overwriteMsgStr, boolean useMsgIdAsSource) {

        String msgStr = createMsgStr(poEntry, resultTitles, overwriteMsgStr, useMsgIdAsSource);

        Entry entry = Entry.builder()
                .isocode(poEntry.getEntry().getIsocode())
                .entryline(msgStr)
                .build();

        log.trace("\n\n*********************\nPoEntry {}, \n\nResult Entry {} ", poEntry, entry);

        return entry;
    }

    private String createMsgStr(PoEntry poEntry, List<String> resultTitles, boolean overwriteMsgStr, boolean useMsgIdAsSource) {

        String entryline = poEntry.getEntry().getEntryline();

        if (isNonValidMsgId(poEntry)) {
            return entryline;

        } else if (hasSource(resultTitles)) {
            if ((overwriteMsgStr && !isMsgStrEmpty(poEntry)) || isMsgStrEmpty(poEntry)) {
                entryline = entryline.replaceFirst(MSGSTR, "msgstr " + QUOTE + resultTitles.get(0) + QUOTE);
            }
        } else if (useMsgIdAsSource) {
            if ((overwriteMsgStr && !isMsgStrEmpty(poEntry)) || isMsgStrEmpty(poEntry)) {
                entryline = entryline.replaceFirst(MSGSTR, "msgstr " + poEntry.getMsgid().trim());

            }
        }
        return entryline;

    /*    } if (isSameResultTitle(poEntry, resultTitle, overwriteMsgStr)) {
            msgStr = poEntry.getMsgid();
        } else {
            if (poEntry.getMsgstr().isEmpty() || poEntry.getMsgstr().equals(QUOTE + QUOTE) || overwriteMsgStr) {
                msgStr = QUOTE + resultTitle + QUOTE;
            } else {
                msgStr = poEntry.getMsgstr();
            }
        }
        return msgStr; */

    }

    private static boolean hasSource(List<String> resultTitles) {
        if (!resultTitles.isEmpty() && !resultTitles.get(0).equalsIgnoreCase(QUOTE + QUOTE)) {
            return true;
        } else {
            return false;
        }
    }

    private void getStarFromLanglink(List<Langlink> langlinks, List<String> msgIdTitles) {
        if (!langlinks.isEmpty()) {
            msgIdTitles.add(langlinks.get(0).getStar().trim());
        }
        ;
    }

    private static boolean isMsgStrEmpty(PoEntry poEntry) {
        return poEntry.getMsgstr().equals(QUOTE + QUOTE) || poEntry.getMsgstr().isEmpty();
    }

    private static boolean isNonValidMsgId(PoEntry poEntry) {
        return asList("", NEWLINE).stream()
                .anyMatch(nonvalid -> nonvalid.equalsIgnoreCase(poEntry.getMsgid()));
    }

    private static boolean isSameResultTitle(PoEntry poEntry, String title, boolean overwrite) {
        if (title.equalsIgnoreCase(poEntry.getMsgid()) || (title.equals("") && overwrite)) {
            return true;
        }
        return false;
    }

}
