package se.hanklank.iso_639_3_translate.service.wikipedia.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Value;

import java.util.Optional;

import static java.util.Optional.ofNullable;

@Value
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Query {

    @JsonProperty("pages")
    @JsonDeserialize(using = PageItemDeserializer.class)
    private final PageItem pageItem;

    public Optional<PageItem> getPageItem() {
        return ofNullable(this.pageItem);
    }
}
