package se.hanklank.iso_639_3_translate.service.wikipedia.api;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

/**
 * Example: https://en.wikipedia.org/w/api.php?action=opensearch&search=japanese|language
 */
public class OpenSearchDeserializer extends JsonDeserializer<OpenSearch> {

    @Override
    public OpenSearch deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {

        Object[] root = jp.readValueAs(Object[].class);

        OpenSearch openSearch = null;

        if (root != null
                && root.length == 4
                && Arrays.asList(root).stream().allMatch(Objects::nonNull)
                && !((ArrayList<String>) root[3]).isEmpty()) {

            openSearch = new OpenSearch(root[0].toString(), (ArrayList<String>) root[1], (ArrayList<String>) root[3]);
        }

        return openSearch;
    }
}