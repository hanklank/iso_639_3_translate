package se.hanklank.iso_639_3_translate.service;

import se.hanklank.iso_639_3_translate.model.Entry;
import se.hanklank.iso_639_3_translate.model.PoEntry;

import java.util.List;

public interface ISO_639_3_Source {

    void process(List<PoEntry> poEntry, List<Entry> entries, boolean overwriteMsgStr, boolean useMsgIdAsSource);
}
