package se.hanklank.iso_639_3_translate.service.kb;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import se.hanklank.iso_639_3_translate.model.KBEntry;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
@Component
public class KBClient {

    private static final Pattern KB_LANGUAGEPOST = Pattern.compile("<tr valign=\"top\">\\s+<td>(.*?)</td>\\s+<td>(.*?)</td>\\s+<td>(.*?)</td>\\s+<td>(.*?)</td>");

    @Value("${kb.url}")
    private String url;


    Map<String, KBEntry> getKbData() {
        try {
            return toMap(Jsoup.connect(url).timeout(15000).get());
        } catch (IOException e) {
            log.error("Couldn't fetch document {}", e);
        }
        return Collections.EMPTY_MAP;
    }

    private static Map<String, KBEntry> toMap(Document document) {
        Matcher matcher = KB_LANGUAGEPOST.matcher(document.getElementById("ContentWide").toString());

        List<KBEntry> kbEntries = new ArrayList<>();

        while (matcher.find()) {
            KBEntry kbEntry = toKbEntry(matcher);
            if (isNotSan(kbEntry)) { //KB has a bug in their list of codes for san, has been reported
                kbEntries.add(kbEntry);
            }
        }
        return kbEntries.stream()
                .collect(Collectors.toMap(KBEntry::getIsocode, Function.identity()));
    }

    private static boolean isNotSan(KBEntry kbEntry) {
        return !kbEntry.getIsocode().equals("san");
    }

    private static KBEntry toKbEntry(Matcher matcher) {
        return KBEntry.builder()
                .isocode(matcher.group(1))
                .swe(matcher.group(2))
                .eng(matcher.group(3))
                .comment(matcher.group(4))
                .build();
    }

}
