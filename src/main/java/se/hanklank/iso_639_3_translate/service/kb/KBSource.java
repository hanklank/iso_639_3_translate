package se.hanklank.iso_639_3_translate.service.kb;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import se.hanklank.iso_639_3_translate.model.Entry;
import se.hanklank.iso_639_3_translate.model.KBEntry;
import se.hanklank.iso_639_3_translate.model.PoEntry;
import se.hanklank.iso_639_3_translate.service.ISO_639_3_Source;

import java.util.List;
import java.util.Map;

import static se.hanklank.iso_639_3_translate.util.Utils.MSGSTR;
import static se.hanklank.iso_639_3_translate.util.Utils.QUOTE;

@RequiredArgsConstructor
@Slf4j
@Component
public class KBSource implements ISO_639_3_Source {

    private final KBClient kbClient;

    @Override
    public void process(List<PoEntry> poEntries, List<Entry> entries, boolean overwriteMsgStr, boolean useMsgIdAsSource) {

        Map<String, KBEntry> kbData = kbClient.getKbData();
        poEntries.stream()
                .forEachOrdered(poEntry -> {
                    entries.add(Entry.builder()
                            .isocode(poEntry.getEntry().getIsocode())
                            .entryline(buildMsgStr(kbData, poEntry, overwriteMsgStr, useMsgIdAsSource))
                            .build());
                });
    }


    private static String buildMsgStr(Map<String, KBEntry> kbSource, PoEntry poEntry, boolean overwriteMsgStr, boolean useMsgIdAsSource) {

        String entryline = poEntry.getEntry().getEntryline();
        System.out.println(poEntry.getMsgstr());

        KBEntry kbEntry = getKbEntry(kbSource, poEntry);

        if (hasKBSource(kbEntry)) {
            if ((overwriteMsgStr && !isMsgStrEmpty(poEntry)) || isMsgStrEmpty(poEntry)) {
                entryline = entryline.replaceFirst(MSGSTR, "msgstr " + QUOTE + kbEntry.getSwe() + QUOTE);
            }
        } else if (useMsgIdAsSource) {
            if ((overwriteMsgStr && !isMsgStrEmpty(poEntry)) || isMsgStrEmpty(poEntry)) {
                entryline = entryline.replaceFirst(MSGSTR, "msgstr " + poEntry.getMsgid().trim());

            }
        }
        return entryline;
    }


    private static boolean hasKBSource(KBEntry kbEntry) {
        return kbEntry != null;
    }

    private static KBEntry getKbEntry(Map<String, KBEntry> kbSource, PoEntry poEntry) {
        return kbSource.get(poEntry.getEntry().getIsocode());
    }

    private static boolean isMsgStrEmpty(PoEntry poEntry) {
        return poEntry.getMsgstr().equals(QUOTE + QUOTE) || poEntry.getMsgstr().isEmpty();
    }
}