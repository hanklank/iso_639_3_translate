package se.hanklank.iso_639_3_translate.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import se.hanklank.iso_639_3_translate.model.SourceType;
import se.hanklank.iso_639_3_translate.service.kb.KBSource;
import se.hanklank.iso_639_3_translate.service.wikipedia.WikipediaSource;

@RequiredArgsConstructor
@Component
public class SourceFactory {

    private final KBSource kbSource;
    private final WikipediaSource wikipediaSource;

    public ISO_639_3_Source getSource(SourceType sourceType) {
        if (sourceType == SourceType.KB) {
            return kbSource;
        } else {
            return wikipediaSource;
        }
    }
}
