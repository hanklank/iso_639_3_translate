package se.hanklank.iso_639_3_translate;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import se.hanklank.iso_639_3_translate.model.Entry;
import se.hanklank.iso_639_3_translate.model.PoEntry;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static se.hanklank.iso_639_3_translate.util.Utils.pathToString;

@Slf4j
@Component
public final class PoHandler {

    private static final Pattern MSGID_PATTERN = Pattern.compile("(.*)for\\s([a-z]{3})(.*)(msgid\\s(.*))(msgstr\\s(.*))", Pattern.DOTALL);
    private static Pattern POENTRY_SPLIT_PATTERN = Pattern.compile("(?=\n\n)");

    public List<PoEntry> toPoEntries(Path path) {

        List<PoEntry> poEntries = new ArrayList<>();
        String[] poFile = POENTRY_SPLIT_PATTERN.split(pathToString(path, StandardCharsets.UTF_8));

        Arrays.stream(poFile)
                .forEachOrdered(line -> {

                    Matcher matcher = MSGID_PATTERN.matcher(line);
                    while (matcher.find()) {
                        poEntries.add(PoEntry.builder()
                                .entry(Entry.builder()
                                        .entryline(matcher.group(0))
                                        .isocode(matcher.group(2)).build())
                                .commentline(matcher.group(1))
                                .msgid(matcher.group(5))
                                .msgstr(matcher.group(7)).build());
                    }
                });
        return poEntries;
    }
}
