package se.hanklank.iso_639_3_translate.util;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@Slf4j
@UtilityClass
public class Utils {

    public static final String QUOTE = "\"";
    public static final String MSGSTR = "msgstr\\s\"(.*)\"";
    public static final String NEWLINE = "\n";

    public static String pathToString(Path path, Charset encoding) {
        String filecontents = "";
        try {
            filecontents = new String(Files.readAllBytes(path), encoding);
        } catch (IOException e) {
            log.error("Couldn't read file {}", e);
        }

        return filecontents;
    }

    public static void write(List<String> strings, Path outpath) throws IOException {
        try (BufferedWriter writer = Files.newBufferedWriter(outpath, StandardCharsets.UTF_8)) {

            strings.forEach(item -> {
                try {
                    writer.write(item);
                } catch (IOException e) {
                    log.error("Couldn't write to file {} due to {} ", outpath, e);
                }
            });
        }
    }
}
