package se.hanklank.iso_639_3_translate.model;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class KBEntry {

    private String isocode;
    private String swe;
    private String eng;
    private String comment;
}
