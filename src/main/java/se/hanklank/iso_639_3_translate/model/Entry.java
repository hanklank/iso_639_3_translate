package se.hanklank.iso_639_3_translate.model;


import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class Entry {
    private String entryline;
    private String isocode;
}
