package se.hanklank.iso_639_3_translate.model;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class PoEntry {

    private String commentline;
    private String msgid;
    private Entry entry;
    private String msgstr;

}
