# ISO 639_3 Translate
> Your iso 639.3 translator helper.

Translate [https://translationproject.org/domain/iso_639_3.html iso 639_3] po file from various sources

![](header.png)

## Prerequisites

Java 8+, Gradle


## Usage example

TO-DO: few motivating and useful examples of how your product can be used. Spice this up with code blocks and potentially more screenshots.

## Development setup

TO-DO:Describe how to install all development dependencies and how to run an automated test-suite of some kind. Potentially do this for multiple platforms.

```sh
make install
npm test
```

## Release History

* 0.1
    * Initial relasee

## Meta

Josef Andersson – [@heapstack](https://twitter.com/heapstack)

Distributed under the GPLv3 license. See ``LICENSE`` for more information.

[https://gitlab.com/hanklank/iso_639_3_translate](https://gitlab.com/hanklank/)




Get msgid
Query wikipedia eng:


https://en.wikipedia.org/w/api.php?action=opensearch&search=japanese|language

Get first answer and go to that page with
https://en.wikipedia.org/w/api.php?action=query&prop=langlinks&titles=Japanses_language&redirects=&lllang=sv

Set the msgstr if found

msgid | eng. wikipedia | sv. wikipedia